package ru.sber.jd;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {


    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {

        logger.error("Сейчас поймаю OutOfMemoryError");

        try {
            Object[] objects = new Object[Integer.MAX_VALUE];
        } catch (Error er) {
            logger.error(er);
            System.out.println("Поймал OutOfMemoryError");
        }

        logger.error("Сейчас поймаю StackOverFlowError");

        try {
            System.out.println(factorial(1));
        } catch (Error er) {
            logger.error(er);
            System.out.println("Поймал StackOverFlowError");
        }

    }


    private static long factorial(int n) {
        return n*factorial(n-1);
    }


}
